#Gitlab Questions :

### Comment protéger une branche dans Gitlab ?
Pour protéger une branche dans Gitlab, il faut se rendre dans les paramètres du projet, puis dans l'onglet "Repository" et enfin dans "Protected branches". Il suffit ensuite de sélectionner la branche que l'on souhaite protéger et de cocher les options que l'on souhaite activer.

---
### Pourquoi protéger des branches dans Git ?
Protéger des branches dans Git permet de s'assurer que personne ne puisse modifier le code sans autorisation. Cela permet de garantir la stabilité du code et d'éviter les erreurs. De plus, cela permet de mettre en place un processus de revue de code et de validation avant de fusionner une branche.

---
### Qu'est ce qui peut entraîner des merge conflicts et comment les résoudre ?
Les merge conflicts peuvent être causés par plusieurs raisons, notamment lorsque deux branches modifient le même fichier ou la même ligne de code. Pour résoudre un conflit de fusion, il faut ouvrir le fichier en conflit et résoudre manuellement les différences. Il est également possible d'utiliser des outils de résolution de conflits pour faciliter le processus.

# Gitlab commands :

## Some git command and explication :

--- 
- `git init` : Creer un nouveau dépôt Git en local
- `git clone` : Clone un dépôt distant dans un nouveau répertoire
- `git remote` : Lie un dépôt distant à un dépôt local
---
- `git add` : Ajoute un fichier à l'index
- `git commit` : Valide les modifications apportées à un fichier
- `git push` : Envoie les modifications effectuées sur une branche locale vers le dépôt distant
---
- `git pull` : Telecharge les modifications effectuées sur une branche distante en local